package Sec_8_ParallelAlgorithms.ParallelSum;

import java.util.List;

public class ParallelWorker extends Thread {

    int start;
    int end;
    List<Long> mainArr;
    long partialSum;

    public ParallelWorker(int start, int end, List<Long> mainArr) {
        this.start = start;
        this.end = end;
        this.mainArr = mainArr;
    }

    public long getPartialSum() {
        return partialSum;
    }

    @Override
    public void run() {
        partialSum = 0;
        for(int i = start; i <= end; i++) {
            partialSum += mainArr.get(i);
        }
    }
}
