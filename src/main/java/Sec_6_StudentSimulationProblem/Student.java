package Sec_6_StudentSimulationProblem;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Student implements Runnable{
    private int id;
    private Book[] books;

    private Random random;

    public Student(int id, Book[] books) {
        this.id = id;
        this.books = books;
        this.random = new Random();
    }


    @Override
    public void run() {
        while(true) {
            int i = random.nextInt(Constants.NO_OF_BOOKS);
            try {
                if (books[i].pickup(this)) {
                    read(i);
                    books[i].putdown(this);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void waitToRead() {
        System.out.println("Student " + this + " is waiting");
    }

    private void read(int i) throws InterruptedException {
        System.out.println("Student " + this + " is reading " + books[i]);
        Thread.sleep(random.nextInt(4));
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
