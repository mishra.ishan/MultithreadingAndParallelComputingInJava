package Sec_3_BasicMultithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProducerAndConsumer {

    private List<Number> list = new ArrayList<>();
    private int value = 0;
    private int low = 0, high = 5;
    private Object lock = new Object();

    private void produce() throws InterruptedException {
        System.out.println("In producer");
        synchronized (lock) {
            while (true) {
                if (list.size() == high) {
                    System.out.println("Producer going to wait for consumer.......");
                    lock.wait();
                } else {
                    list.add(value++);
                    System.out.println("LIST: " + list);
                    lock.notify();
                }
                TimeUnit.MILLISECONDS.sleep(300);
            }
        }
    }

    private void consume() throws InterruptedException {
        System.out.println("In consumer");
        synchronized (lock) {
            while (true) {
                if (list.size() == low) {
                    System.out.println("Consumer going to wait for producer.........");
                    lock.wait();
                } else {
                    list.remove(--value);
                    //value-- will give out of bounds for when value is 5
                    System.out.println("LIST: " + list);
                    lock.notify();
                }
                TimeUnit.MILLISECONDS.sleep(300);
            }
        }
    }

    public static void main(String[] args) {
        final ProducerAndConsumer producerAndConsumer = new ProducerAndConsumer();
        Thread producerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    producerAndConsumer.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread consumerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    producerAndConsumer.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        producerThread.start();
        consumerThread.start();
    }
}
