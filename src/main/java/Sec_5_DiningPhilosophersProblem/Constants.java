package Sec_5_DiningPhilosophersProblem;

public class Constants {
    private Constants() {

    }

    public static final Integer NUMBER_OF_PHILOSOPHERS = 5;
    public static final Integer NUMBER_OF_CHOPSTICKS = 5;
    public static final Long MILLIS_TO_RUN = 20 * 1000l;
}
