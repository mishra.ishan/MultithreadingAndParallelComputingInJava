package Sec_4_ConcurrentCollections;

import java.util.Random;
import java.util.concurrent.*;

public class LinkedBlockingQueues {
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);
        Buffer buffer = new Buffer();
        service.submit(new Producers(buffer));
        service.submit(new Consumers(buffer));
    }
}

class Producers implements Runnable {
    private Buffer buffer;
    Random random = new Random();

    public Producers(Buffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        int i = 20;
        while (i-- > 0) {
            int ele = random.nextInt(10000);
            buffer.addToBuffer(ele);
            System.out.println("Adding to queue: " + ele);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Consumers implements Runnable {
    private Buffer buffer;

    public Consumers(Buffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        while(true) {
            System.out.println("Removing from Queue: " + buffer.removeFromBuffer());
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Buffer {
    BlockingQueue<Integer> blockingQueue = new LinkedBlockingQueue<>();
    //Same code would be for Synchronous and ArrayBlocking Queue. Just put new SynchronousQueue or new ArrayBlocking Queue instead of new LinkedBlockingQueue.

    public void addToBuffer(int ele) {
        try {
            blockingQueue.put(ele);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int removeFromBuffer() {
        try {
            return blockingQueue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Integer.MAX_VALUE;
    }
}