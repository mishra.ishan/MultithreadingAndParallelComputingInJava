package Sec_8_ParallelAlgorithms.ParallelSum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Application {
    public static void main(String[] args) throws InterruptedException {
        Random random = new Random();
        final int ARRAY_SIZE = 100000000;
        List<Long> mainArr = new ArrayList<>();
        for(int i = 0; i < ARRAY_SIZE; i++) {
            Integer num = random.nextInt(ARRAY_SIZE);
            mainArr.add(num.longValue());
        }

        int availableProcessors = Runtime.getRuntime().availableProcessors();
        int chunkSize = (int)Math.ceil(ARRAY_SIZE * 1.0 / availableProcessors);

//        System.out.println("Array: " + Arrays.toString(mainArr));

        long sequentialSum = 0;
        long startTime = System.currentTimeMillis();
        for(int i = 0; i < mainArr.size(); i++) {
            sequentialSum += mainArr.get(i);
        }
        System.out.println("Seq sum: " + sequentialSum + " in " + (System.currentTimeMillis() - startTime));
//        System.out.println("chunk size: " + chunkSize);

        List<ParallelWorker> workers = new ArrayList<>();
        startTime = System.currentTimeMillis();
        for(int i = 0; i < availableProcessors; i++) {

            int end = Math.min(ARRAY_SIZE, (i+1)*chunkSize);

            ParallelWorker worker = new ParallelWorker(i*chunkSize, end-1, mainArr);
            workers.add(worker);
            worker.start();

//            System.out.println("Submitted: " + (i*chunkSize) + "to" + (end -1));

            if(end > ARRAY_SIZE) {
                break;
            }
        }

        long finalSum = 0;
        for(int i = 0; i < workers.size(); i++) {
            ParallelWorker parallelWorker = workers.get(i);
            parallelWorker.join();
            finalSum += parallelWorker.getPartialSum();
        }

        System.out.println("Parallel sum: " + finalSum + " in " + (System.currentTimeMillis() - startTime));
    }
}
