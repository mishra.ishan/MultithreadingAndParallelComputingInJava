package Sec_9_ForkJoinFramework.MaximumNumber;

import java.util.Arrays;

public class MaximumNumberSequential {
    int[] nums;

    public MaximumNumberSequential(int[] nums) {
        this.nums = nums;
    }

    public int compute() {
        int max = nums[0];
        for(int i = 1; i < nums.length; i++) {
            max = Math.max(max, nums[i]);
        }
        return max;
    }
}
