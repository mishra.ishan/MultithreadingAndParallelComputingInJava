package Sec_8_ParallelAlgorithms.ParallelMergeSort;

import java.util.Arrays;

public class MergeSort {

    private int[] tempArr;
    private int[] mainArr;

    public MergeSort(int[] mainArr) {
        this.mainArr = mainArr;
        this.tempArr = new int[mainArr.length];
    }

    private Thread mergeSortParallel (int low, int high, int noOfThreads, String threadName) {
        return new Thread(() -> parallelMergeSort(low, high, noOfThreads/2), threadName);
    }

    public void parallelMergeSort(int low, int high, int noOfThreads) {
        if(noOfThreads <= 1) {
//            System.out.println("Called Sequential merge sort for " + low + "," + high);
            mergeSort(low, high);
            return;
        }

        int middle = (low + high) / 2;

        Thread leftSorter = mergeSortParallel(low, middle, noOfThreads, ("leftSorter:[" + low + "," + middle + "]"));
        Thread rightSorter = mergeSortParallel(middle+1, high, noOfThreads, ("rightSorter:[" + (middle+1) + "," + high + "]"));

        leftSorter.start();
        rightSorter.start();
        try {
            leftSorter.join();
//            System.out.println(leftSorter.getName() + " -> " + Arrays.toString(mainArr));
            rightSorter.join();
//            System.out.println(rightSorter.getName() + " -> " + Arrays.toString(mainArr));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        merge(low, middle, high);
    }

    public void mergeSort(int low, int high) {
        if(low >= high) {
            return;
        }
        int mid = (low + high) / 2;
        mergeSort(low, mid);
        mergeSort(mid+1, high);
        merge(low, mid, high);
    }

    private void merge(int low, int mid, int high) {
        for(int i = low; i <= high; i++) {
            tempArr[i] = mainArr[i];
        }
        int i = low, j = mid+1, k = low;
        while(i <= mid && j <= high) {
            if(tempArr[i] <= tempArr[j]) {
                mainArr[k++] = tempArr[i++];
            }
            else {
                mainArr[k++] = tempArr[j++];
            }
        }

        while(i <= mid) {
            mainArr[k++] = tempArr[i++];
        }
        while(j <= high) {
            mainArr[k++] = tempArr[j++];
        }
    }

    public boolean isSorted() {
        for(int i = 1; i < mainArr.length; i++) {
            if(mainArr[i-1] > mainArr[i]) {
                return false;
            }
        }
        return true;
    }
}
