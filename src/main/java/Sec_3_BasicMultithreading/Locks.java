package Sec_3_BasicMultithreading;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Locks {
    private final Lock lock = new ReentrantLock(true);
    private int val1 = 0;
    private int val2 = 0;

    private void add() {
        System.out.println("[" + Thread.currentThread().getName() + "] About to add");
        lock.lock();
        System.out.println("[" + Thread.currentThread().getName() + "] ADD---XXX");
        try {
            System.out.println("["+Thread.currentThread().getName()+"] ADDING");
            val1++;
        }
        finally {
            System.out.println("[" + Thread.currentThread().getName() + "] ADD---OOO");
            lock.unlock();
        }
    }

    private void addAgain() {
        System.out.println("[" + Thread.currentThread().getName() + "] About to add again");
        lock.lock();
        System.out.println("[" + Thread.currentThread().getName() + "] ADDAGAIN---XXX");
        try {
            System.out.println("["+Thread.currentThread().getName()+"] ADDING AGAIN");
            val2++;
        }
        finally {
            lock.unlock();
            System.out.println("[" + Thread.currentThread().getName() + "] ADDAGAIN---OOO");
        }
    }

    public static void main (String[] args) throws InterruptedException {
        final Locks locks = new Locks();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 1; i++) {
                    locks.add();
                    locks.addAgain();
                }
            }
        }, "T1");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 1; i++) {
                    locks.add();
                    locks.addAgain();
                }
            }
        }, "T2");

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("val1: " + locks.val1);
        System.out.println("val2: " + locks.val2);
    }
}
