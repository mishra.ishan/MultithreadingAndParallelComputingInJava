package Sec_9_ForkJoinFramework.Simple;

import java.util.concurrent.RecursiveAction;

public class SimpleRecursiveAction extends RecursiveAction {

    int workload;

    public SimpleRecursiveAction(int workload) {
        this.workload = workload;
    }

    @Override
    protected void compute() {
        if(workload > 100) {
            System.out.println("Workload is > 100. Splitting it into 2 parts: " + workload);
            int taskCount1 = workload/2;
            int taskCount2 = workload/2;
            if(workload%2 == 1) {
                taskCount2 = workload/2+1;
            }
            SimpleRecursiveAction task1 = new SimpleRecursiveAction(taskCount1);
            SimpleRecursiveAction task2 = new SimpleRecursiveAction(taskCount2);
            task1.fork();
            task2.fork();
        }
        else {
            System.out.println("Workload is < 100. Doing it sequentially......: " + workload);
        }

    }
}
