package Sec_4_ConcurrentCollections;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CyclicBarriers {
    public static void main(String[] args) {
        String name = Thread.currentThread().getName();
        System.out.println("["+ name + "] STARTING......");

        CyclicBarrier barrier = new CyclicBarrier(5, new Runnable() {
            @Override
            public void run() {
                System.out.println("The counter is now 0. All tasks did the needful. Now processing further...");
            }
        });

        ExecutorService service = Executors.newCachedThreadPool();
        for(int i = 0; i < 12; i++) {   //Any multiple of parties number (5) won't block. But if used any other number of iteration, it will cause endless wait cause all threads won't be able to reach the barrier. ,
            service.submit(new Worker1(barrier));
        }
        service.shutdown();
        System.out.println("["+ name + "] ENDING......");
    }
}

class Worker1 implements Runnable {

    private static int count = 0;
    private String id;
    private CyclicBarrier barrier;
    private Random random;

    public Worker1(CyclicBarrier barrier) {
        this.barrier = barrier;
        this.id = "Thread-" + ++count;
        this.random = new Random();
    }

    @Override
    public void run() {
        System.out.println("[" + id + "] Starting...");
        try {
            Thread.sleep(1000l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            barrier.await();
            throw new BrokenBarrierException("Custom");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
            barrier.reset();
        }
        System.out.println("[" + id + "] Ending...");
    }
}
