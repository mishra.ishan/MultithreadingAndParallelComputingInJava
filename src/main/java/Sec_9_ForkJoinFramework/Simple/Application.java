package Sec_9_ForkJoinFramework.Simple;

import java.util.concurrent.ForkJoinPool;

public class Application {

    final int workload = 1020;

    public static void main(String[] args) {


        ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());

        Application application = new Application();
//        application.executeSimpleRecursiveAction(pool);

        application.executeSimpleRecursiveTask(pool);
    }

    public void executeSimpleRecursiveAction(ForkJoinPool pool) {
        SimpleRecursiveAction action = new SimpleRecursiveAction(workload);
        pool.invoke(action);
    }

    public void executeSimpleRecursiveTask(ForkJoinPool pool) {
        SimpleRecursiveTask task = new SimpleRecursiveTask(workload);
        pool.invoke(task);
    }
}
