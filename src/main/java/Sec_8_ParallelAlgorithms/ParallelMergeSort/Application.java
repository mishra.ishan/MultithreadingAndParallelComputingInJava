package Sec_8_ParallelAlgorithms.ParallelMergeSort;

import java.util.Arrays;
import java.util.Random;

public class Application {
    public static void main(String[] args) {
        Random random = new Random();

        final int ARRAY_SIZE = 40000000;
        int[] mainArr = new int[ARRAY_SIZE];
        int[] mainArr2 = new int[ARRAY_SIZE];
        int[] mainArr3 = new int[ARRAY_SIZE];
        int[] mainArr4 = new int[ARRAY_SIZE];


        for(int i = 0; i < ARRAY_SIZE; i++) {
            int element = random.nextInt(mainArr.length);
            mainArr[i] = element;
            mainArr2[i] = element;
            mainArr3[i] = element;
            mainArr4[i] = element;
        }

//        mainArr2 = new int[]{5,2,3,1};
        MergeSort sequentialMergeSort = new MergeSort(mainArr);
        MergeSort parallelMergeSort = new MergeSort(mainArr2);
        MergeSort parallelMergeSort2 = new MergeSort(mainArr3);
        MergeSort parallelMergeSort3 = new MergeSort(mainArr4);


        long startTimestamp = System.currentTimeMillis();


//        System.out.println(Arrays.toString(mainArr));
        sequentialMergeSort.mergeSort(0, mainArr.length-1);
        System.out.println("Time taken to sort sequential: " + (System.currentTimeMillis() - startTimestamp));
//        System.out.println("Sorted array after sequential merge sort: " + Arrays.toString(mainArr));
        System.out.println("Is sorted? : " + sequentialMergeSort.isSorted());



        int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + availableProcessors);

        startTimestamp = System.currentTimeMillis();
        parallelMergeSort.parallelMergeSort(0, mainArr2.length - 1, availableProcessors);


        System.out.println("Time taken to sort parallel with available processors: " + (System.currentTimeMillis() - startTimestamp));
//        System.out.println("Sorted array after merge sort: " + Arrays.toString(mainArr2));
        System.out.println("Is sorted? : " + parallelMergeSort.isSorted());



        startTimestamp = System.currentTimeMillis();
        parallelMergeSort2.parallelMergeSort(0, mainArr3.length - 1, 2 * availableProcessors);

        System.out.println("Time taken to sort parallel with (2 * available) processors: " + (System.currentTimeMillis() - startTimestamp));
//        System.out.println("Sorted array after merge sort: " + Arrays.toString(mainArr3));
        System.out.println("Is sorted? : " + parallelMergeSort2.isSorted());



        startTimestamp = System.currentTimeMillis();
        parallelMergeSort3.parallelMergeSort(0, mainArr4.length - 1, 4 * availableProcessors);

        System.out.println("Time taken to sort parallel with (4 * available) processors: " + (System.currentTimeMillis() - startTimestamp));
//        System.out.println("Sorted array after merge sort: " + Arrays.toString(mainArr3));
        System.out.println("Is sorted? : " + parallelMergeSort3.isSorted());
    }
}
