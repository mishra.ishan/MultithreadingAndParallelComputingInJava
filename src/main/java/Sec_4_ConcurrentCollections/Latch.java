package Sec_4_ConcurrentCollections;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Latch {
    public static void main(String[] args) {
        String name = Thread.currentThread().getName();
        System.out.println("[" + name + "] STARTING......");
        ExecutorService executorService = Executors.newCachedThreadPool();
        CountDownLatch latch = new CountDownLatch(8);
        for (int i = 0; i < 12; i++) {
            executorService.submit(new Worker(latch));
        }
        executorService.shutdown();
        System.out.println("[" + name + "] ENDING......");
    }
}

class Worker implements Runnable {

    private static int counter = 0;
    private String id;
    private CountDownLatch latch;
    private Random random;

    Worker(CountDownLatch latch) {
        this.id = "Thread-" + ++counter;
        this.latch = latch;
        this.random = new Random();
    }

    @Override
    public void run() {
        System.out.println("[" + id + "] Running...");
        try {
            Thread.sleep(1000);
            latch.countDown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("[" + id + "] Finished...");
    }
}
