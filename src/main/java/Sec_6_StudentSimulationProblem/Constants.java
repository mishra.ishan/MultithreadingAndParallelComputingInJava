package Sec_6_StudentSimulationProblem;

public class Constants {

    private Constants() {
    }

    public static final Integer NO_OF_BOOKS = 5;
    public static final Integer NO_OF_STUDENTS = 7;
}
