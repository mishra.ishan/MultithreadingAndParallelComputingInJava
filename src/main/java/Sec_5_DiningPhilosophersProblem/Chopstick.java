package Sec_5_DiningPhilosophersProblem;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Chopstick {
    private int id;
    private Lock lock = new ReentrantLock(true);

    public Chopstick(int id) {
        this.id = id;
    }

    public boolean pickup (Philosopher philosopher, State state) {
        if(lock.tryLock()) {
            System.out.println("Philosopher " + philosopher + " picked up " + state.toString() + " chopstick " + this);
            return true;
        }
        return false;
    }

    public void putdown (Philosopher philosopher, State state) {
        lock.unlock();
        System.out.println("Philosopher " + philosopher + " put down " + state.toString() + " chopstick " + this);
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
