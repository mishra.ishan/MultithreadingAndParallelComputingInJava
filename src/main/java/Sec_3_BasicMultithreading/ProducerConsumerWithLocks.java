package Sec_3_BasicMultithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ProducerConsumerWithLocks {
    private Lock lock = new ReentrantLock(true);
    private List<? extends Number> list = new ArrayList<>();
    private int value = 0, low = 0, high = 5;

    private void produce() {
        try{
            while (true) {
                lock.lock();
            }
        }
        finally {
            lock.unlock();;
        }
    }

    private void consume() {
        try{
            lock.lock();
        }
        finally {
            lock.unlock();;
        }
    }

}
