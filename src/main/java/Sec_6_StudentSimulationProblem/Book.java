package Sec_6_StudentSimulationProblem;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Book {
    private int id;
    private Lock lock;

    public Book(int id) {
        this.id = id;
        this.lock = new ReentrantLock(true);
    }

    public boolean pickup(Student student) throws InterruptedException {
        System.out.println("Student " + student + " trying to pick up " + this);
        if(lock.tryLock(200, TimeUnit.MILLISECONDS)) {
            System.out.println("Student " + student + " picked up " + this);
            return true;
        }
        return false;
    }

    public void putdown(Student student) {
        System.out.println("Student " + student + " putting down " + this);
        lock.unlock();
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}