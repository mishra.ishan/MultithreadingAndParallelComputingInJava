package Sec_5_DiningPhilosophersProblem;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    Philosopher[] philosophers;
    Chopstick[] chopsticks;

    public Philosopher[] getPhilosophers() {
        return philosophers;
    }

    public Application() {
        this.philosophers = new Philosopher[Constants.NUMBER_OF_PHILOSOPHERS];
        this.chopsticks = new Chopstick[Constants.NUMBER_OF_CHOPSTICKS];
        for (int i = 0; i < Constants.NUMBER_OF_CHOPSTICKS; i++) {
            chopsticks[i] = new Chopstick(i);
        }
        for (int i = 0; i < Constants.NUMBER_OF_PHILOSOPHERS; i++) {
            philosophers[i] = new Philosopher(i, chopsticks[i], chopsticks[(i+1)%Constants.NUMBER_OF_CHOPSTICKS]);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Application app = new Application();
        ExecutorService service = Executors.newFixedThreadPool(Constants.NUMBER_OF_PHILOSOPHERS);
        for(int i = 0; i < Constants.NUMBER_OF_PHILOSOPHERS; i++) {
            service.execute(app.getPhilosophers()[i]);
        }

        //Let the application run for sometime.
        Thread.sleep(Constants.MILLIS_TO_RUN);

        //Sentence the application to stop using the isFull flag.
        for(int i = 0; i < Constants.NUMBER_OF_PHILOSOPHERS; i++) {
            app.getPhilosophers()[i].setFull(true);
        }

        service.shutdown();

        //Wait till all the running tasks have shut down properly
        while(!service.isTerminated()) {

        }

        //Print how many times did all philosophers got to eat.
        for(int i = 0; i < Constants.NUMBER_OF_PHILOSOPHERS; i++) {
            System.out.println("Philosopher " + app.getPhilosophers()[i] + " ate " + app.getPhilosophers()[i].getEatCounter());
        }
    }
}
