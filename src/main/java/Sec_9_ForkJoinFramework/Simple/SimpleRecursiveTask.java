package Sec_9_ForkJoinFramework.Simple;

import java.util.concurrent.RecursiveTask;

public class SimpleRecursiveTask extends RecursiveTask<Integer> {

    int workload;

    public SimpleRecursiveTask(int workload) {
        this.workload = workload;
    }

    @Override
    protected Integer compute() {
        if(workload > 100) {
            System.out.println("Workload is > 100. Splitting it into 2 parts: " + workload);
            int taskCount1 = workload/2;
            int taskCount2 = workload/2;
            if(workload%2 == 1) {
                taskCount2 = workload/2+1;
            }
            SimpleRecursiveTask task1 = new SimpleRecursiveTask(taskCount1);
            SimpleRecursiveTask task2 = new SimpleRecursiveTask(taskCount2);
            task1.fork();
            task2.fork();

            return task1.join() + task2.join();
        }
        else {
            System.out.println("Workload is < 100. Doing it sequentially......: " + workload);
            return workload;
        }
    }
}
