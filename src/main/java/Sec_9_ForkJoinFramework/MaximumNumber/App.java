package Sec_9_ForkJoinFramework.MaximumNumber;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class App {
    public static int THRESHOLD;

    public static void main(String[] args) {
        Random random = new Random();
//        int array_nums = 22221111;
        int[] nums = new int[300000000];
        THRESHOLD = nums.length / Runtime.getRuntime().availableProcessors();
        for(int i = 0; i < nums.length; i++) {
            nums[i] = random.nextInt(1000);
        }
        int[] nums2 = new int[nums.length];
        System.arraycopy(nums, 0, nums2, 0, nums.length);

        MaximumNumberSequential sequential = new MaximumNumberSequential(nums);
        long start = System.currentTimeMillis();
        int compute = sequential.compute();
        System.out.println("Max Seq: " + compute + " in " + (System.currentTimeMillis() - start));

        MaximumNumberForkJoin forkJoin = new MaximumNumberForkJoin(0, nums.length, nums2);
        ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
        start = System.currentTimeMillis();
        Integer max = pool.invoke(forkJoin);
        System.out.println("Max Par: " + max + " in " + (System.currentTimeMillis() - start));
    }
}
