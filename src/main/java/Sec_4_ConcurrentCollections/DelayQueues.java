package Sec_4_ConcurrentCollections;

import java.util.concurrent.*;

public class DelayQueues {
    public static void main(String[] args) {
        BlockingQueue<DelayedWorker> queue = new DelayQueue<>();
        try {
            queue.put(new DelayedWorker(10000, "This is the first message"));
            queue.put(new DelayedWorker(100, "This is the second message"));
            queue.put(new DelayedWorker(5000, "This is the third message"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while(!queue.isEmpty()) {
            try {
                System.out.println(queue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class DelayedWorker implements Delayed {

    private long delay;
    private String message;

    public DelayedWorker(long delay, String message) {
        this.delay = delay + System.currentTimeMillis();
        this.message = message;
    }

    @Override
    public long getDelay(TimeUnit timeUnit) {
        return timeUnit.convert(delay - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed delayed) {
        DelayedWorker delayedWorker = (DelayedWorker) delayed;
        if(this.delay < delayedWorker.delay) {
            return 1;   //1: descending order;; -1: ascending order
        }
        if(this.delay > delayedWorker.delay) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "DelayedWorker{" +
                "delay=" + delay +
                ", message='" + message + '\'' +
                '}';
    }
}
