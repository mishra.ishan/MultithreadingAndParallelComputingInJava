package Sec_9_ForkJoinFramework.MaximumNumber;

import java.util.concurrent.RecursiveTask;

public class MaximumNumberForkJoin extends RecursiveTask<Integer> {

    int low;
    int high;
    int[] nums;

    public MaximumNumberForkJoin(int low, int high, int[] nums) {
        this.low = low;
        this.high = high;
        this.nums = nums;
    }

    @Override
    protected Integer compute() {
        if(high - low < App.THRESHOLD) {
//            System.out.println("less number of elements to compute, going for sequential model: " + (high - low));
            int max = Math.max(Integer.MIN_VALUE, nums[low]);
            for(int i = low+1; i < high; i++) {
                max = Math.max(max, nums[i]);
            }
            return max;
        }
        else {
//            System.out.println("Splitting task: ");
            int middle = (low + high)/2;
            MaximumNumberForkJoin leftMaxTask = new MaximumNumberForkJoin(low, middle, nums);
            MaximumNumberForkJoin rightMaxTask = new MaximumNumberForkJoin(middle + 1, high, nums);

            invokeAll(leftMaxTask, rightMaxTask);
//            Integer leftMax = leftMaxTask.invoke();
//            Integer rightMax = rightMaxTask.invoke();

            return Math.max(leftMaxTask.join(), rightMaxTask.join());
        }
    }
}
