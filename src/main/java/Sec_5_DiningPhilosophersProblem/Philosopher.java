package Sec_5_DiningPhilosophersProblem;

import java.util.Random;

public class Philosopher implements Runnable{
    int id;
    //Every philosopher has two chopsticks that he can share.
    private Chopstick leftChopstick, rightChopstick;
    //To keep a count as to how many times did the philosopher got to eat
    private int eatCounter = 0;
    //To think for a random amount of time
    private Random random;
    //If philosopher is full, we need to stop.
    private volatile boolean isFull = false;

    public void eat() throws InterruptedException {
        System.out.println("Philosopher " + id + " is eating...");
        eatCounter++;
        Thread.sleep(random.nextInt(1000));
    }

    public int getEatCounter() {
        return eatCounter;
    }

    public void setFull(boolean full) {
        isFull = full;
    }

    public void think() throws InterruptedException {
        System.out.println("Philosopher " + id + " is thinking");
        Thread.sleep(random.nextInt(1000));
    }

    public Philosopher(int id, Chopstick leftChopstick, Chopstick rightChopstick) {
        this.id = id;
        this.leftChopstick = leftChopstick;
        this.rightChopstick = rightChopstick;
        this.random = new Random();
    }

    @Override
    public void run() {
        while(!isFull) {
            try {
                think();
                if(leftChopstick.pickup(this, State.LEFT)) {
                    if(rightChopstick.pickup(this, State.RIGHT)) {
                        eat();
                        rightChopstick.putdown(this, State.RIGHT);
                    }
                    leftChopstick.putdown(this, State.LEFT);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return  String.valueOf(id);
    }
}
