import java.util.Random;

public class Test {
    public static void main(String[] args) {
        Random random = new Random();
        int i1 = 100000 + random.nextInt(900000);
        int i2 = 100000 + random.nextInt(900000);
        Long val1 = System.currentTimeMillis() + i1;
        Long val2 = System.currentTimeMillis() + i2;

        System.out.println(val1 + " "  + i1);
        System.out.println(val2 + " " + i2);
    }
}
