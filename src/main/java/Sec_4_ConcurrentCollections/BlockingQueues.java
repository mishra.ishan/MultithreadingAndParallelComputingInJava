package Sec_4_ConcurrentCollections;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueues {
    public static void main(String[] args) {
        System.out.println("["+Thread.currentThread().getName()+"] STARTING...");
        BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(5);
        Producer producer = new Producer(queue);
        Consumer consumer = new Consumer(queue);
        Thread t1 = new Thread(producer);
        Thread t2 = new Thread(consumer);
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        System.out.println("["+Thread.currentThread().getName()+"] ENDING...");
    }
}

class Producer implements Runnable {

    private static int count = 0;
    private String id;
    BlockingQueue<Integer> queue;

    public Producer(BlockingQueue<Integer> queue) {
        this.queue = queue;
        this.id = "Thread-" + ++count;
    }

    @Override
    public void run() {
        for(int i = 0; i < 20; i++) {
            try {
                System.out.println("["+id+"] Adding integer: " + i);
                queue.put(i);
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Consumer implements Runnable {

    private static int count = 0;
    private String id;
    BlockingQueue<Integer> queue;

    public Consumer(BlockingQueue<Integer> queue) {
        this.queue = queue;
        this.id = "Thread-" + ++count;
    }

    @Override
    public void run() {
        for(int i = 0; i < 20; i++) {
            System.out.println("["+id+"] Polling integer: " + i);
            try {
                queue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}