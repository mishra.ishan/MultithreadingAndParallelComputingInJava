package Sec_3_BasicMultithreading;

public class SynchronizedBlocks {

    private int count1 = 0;
    private int count2 = 0;

    private Object lock1 = new Object();
    private Object lock2 = new Object();

    private /*synchronized*/ void inc1() {
        synchronized (lock1) {
//            for (int i = 0; i < 100000; i++) {
                count1++;
//            }
        }
    }

    private /*synchronized*/ void inc2() {
        synchronized (lock2) {
//            for (int i = 0; i < 100000; i++) {
                count2++;
//            }
        }
    }
    public static void main (String[] args) {
        final SynchronizedBlocks blocks = new SynchronizedBlocks();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 10000; i++) {
                    blocks.inc1();
                    blocks.inc2();
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 10000; i++) {
                    blocks.inc1();
                    blocks.inc2();
                }
            }
        });
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("count1: " + blocks.count1);
        System.out.println("count2: " + blocks.count2);
    }
}
