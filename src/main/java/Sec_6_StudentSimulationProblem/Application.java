package Sec_6_StudentSimulationProblem;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(Constants.NO_OF_STUDENTS);
        Student[] students = new Student[Constants.NO_OF_STUDENTS];
        Book[] books = new Book[Constants.NO_OF_BOOKS];

        for(int i = 0; i < Constants.NO_OF_BOOKS; i++) {
            books[i] = new Book(i);
        }

        for(int i = 0; i < Constants.NO_OF_STUDENTS; i++) {
            students[i] = new Student(i, books);
            service.execute(students[i]);
        }
    }
}
